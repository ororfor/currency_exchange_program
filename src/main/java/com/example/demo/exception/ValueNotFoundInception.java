package com.example.demo.exception;

public class ValueNotFoundInception extends Exception{
	
	public ValueNotFoundInception(String errorMessage) {
        super(errorMessage);
    }

}
