package com.example.demo.model;

import com.example.demo.util.JsonToStringBuilder;

public class CurrencyDestinationPOJO {
	private String destination_name;
	private double rate;
	public String getDestination_name() {
		return destination_name;
	}
	public void setDestination_name(String destination_name) {
		this.destination_name = destination_name;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	@Override
	public String toString() {
		JsonToStringBuilder builder = new JsonToStringBuilder(this);
		builder.append("destination_name", destination_name);
		builder.append("rate", rate);
		return builder.toString();
	}
	
	
	
}
