package com.example.demo.model;

import java.util.List;

import com.example.demo.util.JsonToStringBuilder;

public class CurrencyPOJO {
	private String source_name;
	private List<CurrencyDestinationPOJO> exchange_rate;
	public String getSource_name() {
		return source_name;
	}
	public void setSource_name(String source_name) {
		this.source_name = source_name;
	}
	public List<CurrencyDestinationPOJO> getExchange_rate() {
		return exchange_rate;
	}
	public void setExchange_rate(List<CurrencyDestinationPOJO> exchange_rate) {
		this.exchange_rate = exchange_rate;
	}
	
	@Override
	public String toString() {
		JsonToStringBuilder builder = new JsonToStringBuilder(this);
		builder.append("source_name", source_name);
		builder.append("exchange_rate", exchange_rate);
		return builder.toString();
	}

	
		

}
