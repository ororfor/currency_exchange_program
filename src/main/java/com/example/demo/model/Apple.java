package com.example.demo.model;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.exception.ValueNotFoundInception;
import com.example.demo.service.ExchangeService;
import com.fasterxml.jackson.databind.JsonMappingException;

@Component
public class Apple {
	
	final static Logger logger = Logger.getLogger(Dollar.class);	
	private double dollar;
	private double apple;
	private double banana;
	
	private int conversionIn;

	@Autowired
	private ExchangeService exchangeService ;

	@Autowired
	public Apple(int conversionIn , ExchangeService exchangeService)  {
		this.conversionIn = conversionIn; 
		this.exchangeService = exchangeService;
	
		try {
			this.setApple(this.conversionIn);
			this.setDollar(this.conversionIn);	
			this.setBanana(this.conversionIn);
		} catch (JsonMappingException | InvalidInputException | ValueNotFoundInception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
		}
		
	}
	public Apple() {
		
	}
	
	public double getDollar() {
		return dollar;
	}

	public void setDollar(double dollar) throws InvalidInputException, JsonMappingException, ValueNotFoundInception {
		this.dollar = exchangeService.exchangeCurrency("apple", "dollar", this.getApple()) ;
		
	}

	public double getApple() throws JsonMappingException, InvalidInputException, ValueNotFoundInception {
		return apple;
	}

	public void setApple(double apple) throws JsonMappingException, InvalidInputException, ValueNotFoundInception  {
		if (apple < 0)
			throw new InvalidInputException("Value must more than zero");
		else
			this.apple = apple;
	}

	public double getBanana() {
		return banana;
	}

	public void setBanana(double banana) throws JsonMappingException, InvalidInputException, ValueNotFoundInception {
		this.banana = exchangeService.exchangeCurrency("apple", "banana", this.getApple()) ;
	}
	

}
