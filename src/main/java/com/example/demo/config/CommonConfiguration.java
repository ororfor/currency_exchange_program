package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.exception.ValueNotFoundInception;
import com.example.demo.model.Apple;
import com.example.demo.model.Banana;
import com.example.demo.model.Dollar;
import com.example.demo.service.ExchangeService;
import com.example.demo.service.ExchangeServiceImpl;
import com.fasterxml.jackson.databind.JsonMappingException;

@Configuration
public class CommonConfiguration {
	
	@Bean
    public int conversionIn(){
        return 0;
    }
	
	@Primary
	@Bean
	public ExchangeService constructExchangeService() {
		return new ExchangeServiceImpl();
	}
	
	@Bean
	public Dollar constructDollar(int conversionIn , ExchangeService exchangeService) throws JsonMappingException, InvalidInputException, ValueNotFoundInception {
		return new Dollar(conversionIn, exchangeService);
	}
	
	@Bean
	public Apple constructApple(int conversionIn , ExchangeService exchangeService) throws JsonMappingException, InvalidInputException, ValueNotFoundInception {
		return new Apple(conversionIn, exchangeService);
	}
	
	@Bean
	public Banana constructBanana(int conversionIn , ExchangeService exchangeService) throws JsonMappingException, InvalidInputException, ValueNotFoundInception {
		return new Banana(conversionIn, exchangeService);
	}
	
	

}
