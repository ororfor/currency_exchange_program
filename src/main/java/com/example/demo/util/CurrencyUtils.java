package com.example.demo.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.util.FileCopyUtils;

import com.example.demo.model.CurrencyPOJO;
import com.example.demo.service.CurrencyServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CurrencyUtils {
	
	final static Logger logger = Logger.getLogger(CurrencyUtils.class);

	public static String convertResourceAsJsonString(Resource resource) {

		try {
			
			InputStream inputStream = resource.getInputStream();
			byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
			String data = new String(bdata, StandardCharsets.UTF_8);
			return data;
		} catch (IOException e) {
			logger.error("IOException", e);
			throw new UncheckedIOException(e);

		}
	}

	public static List<CurrencyPOJO> convertJsonStringAsCurrencyList(String jsonString) throws JsonMappingException {
		ObjectMapper mapper = new ObjectMapper();
		List<CurrencyPOJO> currencyList = null;
		try {
			currencyList = Arrays.asList(mapper.readValue(jsonString, CurrencyPOJO[].class));
		} catch (JsonMappingException e) {
			logger.error("Invalid Json format");
			e.printStackTrace();
			throw new JsonMappingException("Invalid Json format");
		} catch (JsonProcessingException e) {
			logger.error("JsonProcessingException");
			e.printStackTrace();
		}

		return currencyList;
	}
}
