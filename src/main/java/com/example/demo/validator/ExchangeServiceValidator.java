package com.example.demo.validator;

import com.example.demo.exception.InvalidInputException;

public interface ExchangeServiceValidator {
	public void validateConversionIn(double conversionIn) throws InvalidInputException ;
}
