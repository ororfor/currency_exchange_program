package com.example.demo.validator;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.example.demo.exception.InvalidInputException;


@Service
public class ExchangeServiceValidatorImpl implements ExchangeServiceValidator{
	
	final static Logger logger = Logger.getLogger(ExchangeServiceValidatorImpl.class);

	@Override
	public void validateConversionIn(double conversionIn) throws InvalidInputException {
		logger.info("Validate Input parameters.");
				
		if(conversionIn < 0) {
			throw new InvalidInputException("Value must more than zero");
		}
	}
		
		
	
	
}
