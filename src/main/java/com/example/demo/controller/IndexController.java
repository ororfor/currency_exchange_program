package com.example.demo.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.exception.ValueNotFoundInception;
import com.example.demo.model.Apple;
import com.example.demo.model.Banana;
import com.example.demo.model.Dollar;
import com.example.demo.service.ExchangeService;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
@RequestMapping(value = "/api")
public class IndexController {
	
	final static Logger logger = Logger.getLogger(IndexController.class);
		
	@Autowired
	ExchangeService exchangeService;
	

	@RequestMapping(value={"/index"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
	public void test() throws InvalidInputException, ValueNotFoundInception, JsonMappingException {
		
		
		Dollar dollar = new Dollar(20, exchangeService);	
		logger.info("Dollar->Apple : " + dollar.getApple());
		logger.info("Dollar->Banana : " + dollar.getBanana());
		logger.info("Dollar->Dollar : " + dollar.getDollar());
		
		Apple apple = new Apple(5, exchangeService);	
		logger.info("Apple->Dollar : " + apple.getDollar());
		logger.info("Apple->Banana : " + apple.getBanana());
		logger.info("Apple->Apple : " + apple.getApple());
		
		Banana banana = new Banana(5, exchangeService);	
		logger.info("Banana->Dollar : " + banana.getDollar());
		logger.info("Banana->Apple : " + banana.getApple());
		logger.info("Banana->Banana : " + banana.getBanana());
		
		
	
	}

}
