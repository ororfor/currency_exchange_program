package com.example.demo.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.exception.ValueNotFoundInception;
import com.example.demo.model.CurrencyDestinationPOJO;
import com.example.demo.model.CurrencyPOJO;
import com.example.demo.util.CurrencyUtils;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class CurrencyServiceImpl implements CurrencyService {

	final static Logger logger = Logger.getLogger(CurrencyServiceImpl.class);

	@Value("${currencyJsonPath}")
	Resource resource;

	@Override
	public List<CurrencyPOJO> getAllCurrency() throws JsonMappingException {

		String jsonString = CurrencyUtils.convertResourceAsJsonString(resource);
		List<CurrencyPOJO> currencyList = CurrencyUtils.convertJsonStringAsCurrencyList(jsonString);
		logger.info("Currency List : " + currencyList.toString());

		return currencyList;
	}

	@Override
	public double getCurrencyRateBySourceAndDestination(String sourceCurrency, String destinationCurrency)
			throws InvalidInputException, ValueNotFoundInception, JsonMappingException {
		if (StringUtils.isEmpty(sourceCurrency) || StringUtils.isEmpty(destinationCurrency)) {
			logger.error("Source or Destination Currency must not Empty or Null");
			throw new InvalidInputException("Source or Destination Currency must not Empty or Null");
		}
		String jsonString = CurrencyUtils.convertResourceAsJsonString(resource);
		List<CurrencyPOJO> currencyList = CurrencyUtils.convertJsonStringAsCurrencyList(jsonString);
		double exchange_rate = -1;
		for (CurrencyPOJO currency : currencyList) {
			if (currency.getSource_name().equals(sourceCurrency)) {
				for (CurrencyDestinationPOJO exchangeRateObj : currency.getExchange_rate()) {
					if (exchangeRateObj.getDestination_name().equals(destinationCurrency)) {
						exchange_rate = exchangeRateObj.getRate();
					}
				}
			}
		}
		if (exchange_rate == -1) {
			logger.error("Source or Destination Currency not found in currency resource file");
			throw new ValueNotFoundInception("Source or Destination Currency not found in currency resource file");
		}
		logger.info("Exchange rate for " + sourceCurrency + " to " + destinationCurrency + " is " + exchange_rate);
		return exchange_rate;
	}

}
