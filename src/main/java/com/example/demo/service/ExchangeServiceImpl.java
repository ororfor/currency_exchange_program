package com.example.demo.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.exception.ValueNotFoundInception;
import com.example.demo.validator.ExchangeServiceValidator;
import com.fasterxml.jackson.databind.JsonMappingException;

@Service
public class ExchangeServiceImpl implements ExchangeService {
	
	final static Logger logger = Logger.getLogger(ExchangeServiceImpl.class);

	@Autowired
	CurrencyService currencyService;

	@Autowired
	ExchangeServiceValidator exchangeServiceValidator;

	@Override
	public double exchangeCurrency(String sourceCurrency, String destinationCurrency, double conversionIn)
			throws InvalidInputException, JsonMappingException, ValueNotFoundInception {
		exchangeServiceValidator.validateConversionIn(conversionIn);
		double conversionOut = currencyService.getCurrencyRateBySourceAndDestination(sourceCurrency,
				destinationCurrency) * conversionIn;
		logger.info("Exchange "+ sourceCurrency + " "+ conversionIn + " to " + conversionOut + " " +destinationCurrency);
		return conversionOut;
	}

}
