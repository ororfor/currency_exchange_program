package com.example.demo.service;

import java.util.List;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.exception.ValueNotFoundInception;
import com.example.demo.model.CurrencyPOJO;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface CurrencyService {

	public List<CurrencyPOJO> getAllCurrency() throws JsonMappingException;
	public double getCurrencyRateBySourceAndDestination(String sourceCurrency, String destinationCurrency) throws InvalidInputException, ValueNotFoundInception, JsonMappingException;
}
