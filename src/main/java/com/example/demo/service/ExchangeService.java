
package com.example.demo.service;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.exception.ValueNotFoundInception;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface ExchangeService {	
	public double exchangeCurrency(String sourceCurrency, String destinationCurrency, double conversionIn)
			throws InvalidInputException, JsonMappingException, ValueNotFoundInception;
}
