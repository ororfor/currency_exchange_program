package com.example.demo;

import org.apache.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.example.demo")
public class CurrencyExchangeProgramApplication {
	final static Logger logger = Logger.getLogger(CurrencyExchangeProgramApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(CurrencyExchangeProgramApplication.class, args);
	}

}
