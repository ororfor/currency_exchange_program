package com.example.demo.validator;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import com.example.demo.exception.InvalidInputException;

class ExchangeServiceValidatorImplTest {
	
	@InjectMocks
	ExchangeServiceValidator exchangeServiceValidator = new ExchangeServiceValidatorImpl();

	@Test
	void should_throws_InvalidInputException_when_inputLessThanZero() throws Exception {
			
		Executable executable = () -> exchangeServiceValidator.validateConversionIn(-1);
		Exception exception = Assertions.assertThrows(InvalidInputException.class, executable);
		assertEquals("Value must more than zero", exception.getMessage());
		
	}
	
}
