package com.example.demo.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.example.demo.service.ExchangeService;

class BananaTest {

	@Mock
	ExchangeService exchangeService;

	@InjectMocks
	Banana banana;
	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void should_return_0_5_when_getDollar_with_5_banana() throws Exception {
		
		Mockito.when(exchangeService.exchangeCurrency("banana", "dollar", 5)).thenReturn(0.5);
		banana = new Banana(5, exchangeService);
		assertEquals(0.5, banana.getDollar());

	}
	
	@Test
	void should_return_2_50_when_getApple_with_5_banana() throws Exception {
		
		Mockito.when(exchangeService.exchangeCurrency("banana", "apple", 5)).thenReturn(2.5);
		banana = new Banana(5, exchangeService);
		assertEquals(2.5, banana.getApple());

	}
}
