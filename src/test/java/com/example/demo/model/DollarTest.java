package com.example.demo.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.example.demo.service.ExchangeService;

class DollarTest {

	@Mock
	ExchangeService exchangeService;

	@InjectMocks
	Dollar dollar;
	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void should_return_100_when_getApple_with_20_dollar() throws Exception {
		
		Mockito.when(exchangeService.exchangeCurrency("dollar", "apple", 20)).thenReturn(100.0);
		dollar = new Dollar(20, exchangeService);
		assertEquals(100, dollar.getApple());

	}
	
	@Test
	void should_return_200_when_getBanana_with_20_dollar() throws Exception {
		
		Mockito.when(exchangeService.exchangeCurrency("dollar", "banana", 20)).thenReturn(200.0);
		dollar = new Dollar(20, exchangeService);
		assertEquals(200, dollar.getBanana());

	}
	

}
