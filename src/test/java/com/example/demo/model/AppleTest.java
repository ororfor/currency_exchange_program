package com.example.demo.model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.example.demo.service.ExchangeService;

class AppleTest {

	@Mock
	ExchangeService exchangeService;

	@InjectMocks
	Apple apple;
	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void should_return_1_when_getDollar_with_5_apple() throws Exception {
		
		Mockito.when(exchangeService.exchangeCurrency("apple", "dollar", 5)).thenReturn(1.0);
		apple = new Apple(5, exchangeService);
		assertEquals(1, apple.getDollar());

	}
	
	@Test
	void should_return_10_when_getBanana_with_5_apple() throws Exception {
		
		Mockito.when(exchangeService.exchangeCurrency("apple", "banana", 5)).thenReturn(10.0);
		apple = new Apple(5, exchangeService);
		assertEquals(10, apple.getBanana());

	}
	

}
