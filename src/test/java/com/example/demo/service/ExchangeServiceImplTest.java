package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.validator.ExchangeServiceValidator;

class ExchangeServiceImplTest {
	
	@Mock
	private CurrencyService currencyService;
	
	@Mock
	private ExchangeServiceValidator exchangeServiceValidator;
	
	@InjectMocks
	private ExchangeService exchangeService = new ExchangeServiceImpl();
	
	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void should_throws_InvalidInputException_when_inputLessThanZero() throws Exception {
		
		Mockito.doThrow(new InvalidInputException("Value must more than zero")).when(exchangeServiceValidator).validateConversionIn(-1);
		Mockito.when(this.currencyService.getCurrencyRateBySourceAndDestination("apple", "dollar")).thenReturn(0.20);
		Executable executable = () -> exchangeService.exchangeCurrency("apple", "dollar", -1);
		Exception exception = Assertions.assertThrows(InvalidInputException.class, executable);
		assertEquals("Value must more than zero", exception.getMessage());
		
	}
	
	@Test
	void should_return_10_when_exchange_apple_dollar_50() throws Exception {
		
		Mockito.when(this.currencyService.getCurrencyRateBySourceAndDestination(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(0.20);
		assertEquals(10, exchangeService.exchangeCurrency("apple", "dollar", 50));
		
		
	}
	
	@Test
	void should_return_25_when_exchange_banana_apple_50() throws Exception {
		
		Mockito.when(this.currencyService.getCurrencyRateBySourceAndDestination(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(0.5);
		assertEquals(25, exchangeService.exchangeCurrency("banana", "apple", 50));
		
		
	}
	
	@Test
	void should_return_250_when_exchange_dollar_apple_50() throws Exception {
		
		Mockito.when(this.currencyService.getCurrencyRateBySourceAndDestination(ArgumentMatchers.anyString(), ArgumentMatchers.anyString())).thenReturn(5.0);
		assertEquals(250, exchangeService.exchangeCurrency("dollar", "apple", 50));
		
		
	}

}
