package com.example.demo.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.core.io.Resource;

import com.example.demo.exception.InvalidInputException;
import com.example.demo.exception.ValueNotFoundInception;
import com.example.demo.model.CurrencyPOJO;
import com.fasterxml.jackson.databind.JsonMappingException;

class CurrencyServiceImplTest {

	final static Logger logger = Logger.getLogger(CurrencyServiceImplTest.class);	
	@Mock
	private Resource resource;
	
	@InjectMocks
	private CurrencyService currencyService = new CurrencyServiceImpl();

	@BeforeEach
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	
	@Test
	void should_returnActualCurrencyList_when_loadAllCurrency() throws Exception {
		String initialString = "[{\n" + 
				"	\"source_name\" : \"dollar\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 5\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 10\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"apple\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.20\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 2\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"banana\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.10\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 0.5\n" + 
				"		}]\n" + 
				"}]";
	    InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
		Mockito.when(this.resource.getInputStream()).thenReturn(targetStream);
		

		List<CurrencyPOJO> currencyList = currencyService.getAllCurrency();
		
		assertNotNull(currencyList);
		assertEquals(3, currencyList.size());
		assertEquals("dollar", currencyList.get(0).getSource_name());
		assertEquals(2, currencyList.get(0).getExchange_rate().size());
		assertEquals("apple", currencyList.get(0).getExchange_rate().get(0).getDestination_name());
		assertEquals(0.20, currencyList.get(1).getExchange_rate().get(0).getRate());
		assertEquals("apple", currencyList.get(2).getExchange_rate().get(1).getDestination_name());
		
		
	}
	

	@Test
	void should_returnActualCurrencyRate_when_getCurrencyRateBySpecificCurrency() throws Exception {
		String initialString = "[{\n" + 
				"	\"source_name\" : \"dollar\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 5\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 10\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"apple\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.20\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 2\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"banana\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.10\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 0.5\n" + 
				"		}]\n" + 
				"}]";
	    InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
		Mockito.when(this.resource.getInputStream()).thenReturn(targetStream);	
		assertEquals(0.5, currencyService.getCurrencyRateBySourceAndDestination("banana", "apple"));
				
	}
	
	@Test
	void should_throws_InvalidInputException_when_inputNullParameter() throws Exception {
		String initialString = "[{\n" + 
				"	\"source_name\" : \"dollar\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 5\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 10\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"apple\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.20\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 2\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"banana\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.10\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 0.5\n" + 
				"		}]\n" + 
				"}]";
	    InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
		Mockito.when(this.resource.getInputStream()).thenReturn(targetStream);	
		Executable executable = () -> currencyService.getCurrencyRateBySourceAndDestination(null, "apple");
		Exception exception = Assertions.assertThrows(InvalidInputException.class, executable);
		assertEquals("Source or Destination Currency must not Empty or Null", exception.getMessage());
		
	}
	
	@Test
	void should_throws_InvalidInputException_when_inputEmptyParameter() throws Exception {
		String initialString = "[{\n" + 
				"	\"source_name\" : \"dollar\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 5\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 10\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"apple\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.20\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 2\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"banana\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.10\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 0.5\n" + 
				"		}]\n" + 
				"}]";
	    InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
		Mockito.when(this.resource.getInputStream()).thenReturn(targetStream);	
		Executable executable = () -> currencyService.getCurrencyRateBySourceAndDestination("apple", "");
		Exception exception = Assertions.assertThrows(InvalidInputException.class, executable);
		assertEquals("Source or Destination Currency must not Empty or Null", exception.getMessage());
		
	}
	
	@Test
	void should_throws_InvalidInputException_when_inputWrongCurrency() throws Exception {
		String initialString = "[{\n" + 
				"	\"source_name\" : \"dollar\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 5\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 10\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"apple\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.20\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 2\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"banana\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.10\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 0.5\n" + 
				"		}]\n" + 
				"}]";
	    InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
		Mockito.when(this.resource.getInputStream()).thenReturn(targetStream);	
		Executable executable = () -> currencyService.getCurrencyRateBySourceAndDestination("banana", "orange");
		Exception exception = Assertions.assertThrows(ValueNotFoundInception.class, executable);
		assertEquals("Source or Destination Currency not found in currency resource file", exception.getMessage());
		
	}
	
	@Test
	void should_throws_JsonMappingException_when_loadWrongJsonFormat() throws Exception {
		String initialString = "[{\n" + 
				"	\"source_name\" : \"dollar\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 5\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 10\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"apple\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.20\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"banana\", \n" + 
				"		\"rate\" : 2\n" + 
				"		}]\n" + 
				"},{\n" + 
				"	\"source_name\" : \"banana\",\n" + 
				"	\"exchange_rate\" : [{ \n" + 
				"		\"destination_name\" : \"dollar\", \n" + 
				"		\"rate\" : 0.10\n" + 
				"		},{\n" + 
				"		\"destination_name\" : \"apple\", \n" + 
				"		\"rate\" : 0.5\n" + 
				"		}]\n" + 
				"}";
	    InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
		Mockito.when(this.resource.getInputStream()).thenReturn(targetStream);	
		Executable executable = () -> currencyService.getCurrencyRateBySourceAndDestination("banana", "orange");
		Exception exception = Assertions.assertThrows(JsonMappingException.class, executable);
		assertEquals("Invalid Json format", exception.getMessage());
		
	}
	@Test
	void should_throws_JsonMappingException_when_loadEmptyJson() throws Exception {
		String initialString = "";
	    InputStream targetStream = new ByteArrayInputStream(initialString.getBytes());
		Mockito.when(this.resource.getInputStream()).thenReturn(targetStream);	
		Executable executable = () -> currencyService.getCurrencyRateBySourceAndDestination("banana", "orange");
		Exception exception = Assertions.assertThrows(JsonMappingException.class, executable);
		assertEquals("Invalid Json format", exception.getMessage());
		
	}
	
	@Test
	void should_throws_JsonMappingException_when_loadNullJson() throws Exception {
		Mockito.when(this.resource.getInputStream()).thenReturn(null);	
		Executable executable = () -> currencyService.getCurrencyRateBySourceAndDestination("banana", "orange");
		Exception exception = Assertions.assertThrows(JsonMappingException.class, executable);
		assertEquals("Invalid Json format", exception.getMessage());
		
	}
	
	

}
